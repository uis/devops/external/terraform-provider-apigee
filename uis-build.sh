#!/bin/bash

function build() {
    export GOOS=$1
    export GOARCH=$2
    go build -o build/registry.terraform.io/scastria/apigee/$VERSION/${GOOS}_${GOARCH}/terraform-provider-apigee_v$VERSION
}

if [ -z "$VERSION" ]; then
    echo "VERSION must be set"
    exit 1
fi

if [ -d "build" ]; then
    rm -r build
fi

build darwin amd64
build darwin arm64
build linux amd64

pushd build
zip -r ./terraform-provider-apigee_$VERSION.zip .
popd
