# terraform-provider-apigee

Latest documentation can be found here: https://registry.terraform.io/providers/scastria/apigee/latest/docs

## UIS-local fork

This repository contains a UIS-local fork of
https://github.com/scastria/terraform-provider-apigee. The `main` branch is
automatically kept in sync with the upstream branch.

To use this version:

1. Download the latest zip-ed build artefact from the latest [pipeline
   run](https://gitlab.developers.cam.ac.uk/uis/devops/external/terraform-provider-apigee/-/pipelines).
   The artifacts can be found by opening the `build` job log and browsing via
   the sidebar. The zip will be named something like
   `terraform-provider-apigee_0.1.42+uis1.zip`
2. In the root of the terraform configuration which is to make use of this
   version, create a terraform plugin dir and unzip:
   ```console
   $ mkdir -p terraform.d/plugins/
   $ unzip /path/to/terraform-provider-apigee_0.1.42+uis1.zip -d terraform.d/plugins/
   ```
3. Make sure to set the required version of the provider in `versions.tf` to
   match. For example: `0.1.42+uis1`.
4. Run `terraform init -upgrade` to make use of the new version.

The CI job builds providers for Intel Macs, M1 Macs and Intel Linux boxes.
